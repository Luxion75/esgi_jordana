FROM debian:jessie

RUN apt-get update && apt-get install -y git=1:2.1.4-2.1+deb8u10 --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y ansible=1.7.2+dfsg-2+deb8u3 --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*
